package net.illusionsmc.survivalgames.timers.deathmatch;

import com.portalmc.engine.utils.countdowns.Countdown;
import com.portalmc.engine.utils.countdowns.CountdownHandler;
import lombok.Data;
import lombok.Getter;
import me.confuser.barapi.BarAPI;
import net.illusionsmc.survivalgames.arena.SpawnLocations;
import net.illusionsmc.survivalgames.timers.endgame.Endgame;
import net.illusionsmc.survivalgames.utils.GamePlayer;
import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.MapManager;
import net.illusionsmc.survivalgames.utils.Spectator;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import static com.portalmc.engine.utils.Formatter.formatTime;
import static net.illusionsmc.survivalgames.utils.Messages.getFormat;
import static net.illusionsmc.survivalgames.utils.ScoreboardManager.updateBoard;

/**
 * Created by Joey on 7/27/2014.
 */
@Data
public class Predeathmatch implements CountdownHandler {

    @Getter private static Predeathmatch instance;

    public static void startPredeathmatch() {
        Countdown cd = new Countdown(new Predeathmatch(Predeathmatch.getInstance()), 60);
        cd.startCoundown();
    }

    private final Predeathmatch predmCD;
    @Override
    public void countdownStarting(Integer maxSeconds, Countdown countdown) {
        for(Player pl : Bukkit.getOnlinePlayers()) {
            BarAPI.setMessage(pl, "§cPreDeathmatch §8| §660 seconds");
        }
        updateBoard("§cPreDeathmatch", "60 seconds", GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
    }
    @Override
    public void countdownChanged(Integer maxSeconds, Integer timeRemaining, Countdown countdown) {
        if(GamePlayer.getPlayersSize() <= 1) {
            countdown.pauseCountdown();
            Endgame.startEndgame();
            GameState.setState(GameState.ENDGAME);
        }else {
            for (Player pl : Bukkit.getOnlinePlayers()) {
                BarAPI.setMessage(pl, "§cPreDeathmatch §8| §6" + formatTime(timeRemaining));
            }
            updateBoard("§cPreDeathmatch", formatTime(timeRemaining), GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
        }
    }
    @Override
    public void countdownComplete(Integer maxSeconds, Countdown countdown) {
        SpawnLocations.teleportPlayersToMap(MapManager.map);
        GameState.setState(GameState.DEATHMATCH);
        Deathmatch.startDeathmatch();
        Bukkit.broadcastMessage(getFormat("formats.deathmatch-started", true));
    }

}
