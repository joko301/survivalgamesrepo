package net.illusionsmc.survivalgames.timers.deathmatch;

import com.portalmc.engine.utils.countdowns.Countdown;
import com.portalmc.engine.utils.countdowns.CountdownHandler;
import lombok.Data;
import lombok.Getter;
import me.confuser.barapi.BarAPI;
import net.illusionsmc.survivalgames.timers.endgame.Endgame;
import net.illusionsmc.survivalgames.utils.GamePlayer;
import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.Spectator;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import static com.portalmc.engine.utils.Formatter.formatTime;
import static net.illusionsmc.survivalgames.utils.Messages.getFormat;
import static net.illusionsmc.survivalgames.utils.ScoreboardManager.updateBoard;

/**
 * Created by Joey on 7/27/2014.
 */
@Data
public class Deathmatch implements CountdownHandler {

    @Getter
    private static Deathmatch instance;

    public static void startDeathmatch() {
        Countdown cd = new Countdown(new Deathmatch(Deathmatch.getInstance()), 180);
        cd.startCoundown();
    }

    private final Deathmatch deathmatchCD;
    @Override
    public void countdownStarting(Integer maxSeconds, Countdown countdown) {
        for(Player pl : Bukkit.getOnlinePlayers()) {
            BarAPI.setMessage(pl, "§cDeathmatch §8| §63 minutes");
        }
        updateBoard("§cDeathmatch", "3:00 minutes", GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
    }
    @Override
    public void countdownChanged(Integer maxSeconds, Integer timeRemaining, Countdown countdown) {
        if(GamePlayer.getPlayersSize() <= 1) {
            countdown.pauseCountdown();
            Endgame.startEndgame();
            GameState.setState(GameState.ENDGAME);
        }else {
            for (Player pl : Bukkit.getOnlinePlayers()) {
                BarAPI.setMessage(pl, "§cDeathmatch §8| §6" + formatTime(timeRemaining));
            }
            updateBoard("§cDeathmatch", formatTime(timeRemaining), GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
        }
    }
    @Override
    public void countdownComplete(Integer maxSeconds, Countdown countdown) {
        Endgame.startEndgame();
        GameState.setState(GameState.ENDGAME);
        Bukkit.broadcastMessage(getFormat("formats.game-ending", true));
    }

}
