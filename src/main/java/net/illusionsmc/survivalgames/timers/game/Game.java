package net.illusionsmc.survivalgames.timers.game;

import com.portalmc.engine.utils.countdowns.Countdown;
import com.portalmc.engine.utils.countdowns.CountdownHandler;
import lombok.Data;
import lombok.Getter;
import me.confuser.barapi.BarAPI;
import net.illusionsmc.survivalgames.SurvivalGames;
import net.illusionsmc.survivalgames.timers.deathmatch.Deathmatch;
import net.illusionsmc.survivalgames.timers.deathmatch.Predeathmatch;
import net.illusionsmc.survivalgames.timers.endgame.Endgame;
import net.illusionsmc.survivalgames.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import static com.portalmc.engine.utils.Formatter.formatTime;
import static net.illusionsmc.survivalgames.utils.ChestFiller.fill;
import static net.illusionsmc.survivalgames.utils.Messages.getFormat;
import static net.illusionsmc.survivalgames.utils.ScoreboardManager.updateBoard;

/**
 * Created by Joey on 7/26/2014.
 */
@Data
public final class Game implements CountdownHandler {

    @Getter
    private static Game instance;
    @Getter private static Integer timeLeft;
    private static Countdown cd = new Countdown(new Game(Game.getInstance()), 1200);

    public static void startGame() {
        cd.startCoundown();
    }
    public static void stopGame() {
        cd.pauseCountdown();
    }

    private final Game gameCD;
    @Override
    public void countdownStarting(Integer maxSeconds, Countdown countdown) {
        for(Player pl : Bukkit.getOnlinePlayers()) {
            BarAPI.setMessage(pl, "§6In-Game §8| §620:00 minutes Until Deathmatch");
        }
        updateBoard("§6In-Game", "20:00 minutes", GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
    }
    @Override
    public void countdownChanged(Integer maxSeconds, Integer timeRemaining, Countdown countdown) {
        if(GamePlayer.getPlayersSize() <= 1) {
            countdown.pauseCountdown();
            Endgame.startEndgame();
            GameState.setState(GameState.ENDGAME);
        }else if(GamePlayer.getPlayersSize() == 4) {
            countdown.pauseCountdown();
            Bukkit.getScheduler().runTaskLater(SurvivalGames.getInstance(), new Runnable() {
                @Override
                public void run() {
                    Predeathmatch.startPredeathmatch();
                    Bukkit.broadcastMessage(getFormat("formats.deathmatch-starting", true));
                }
            }, 20L);
        }
        else {
            for (Player pl : Bukkit.getOnlinePlayers()) {
                BarAPI.setMessage(pl, "§6In-Game §8| §6" + formatTime(timeRemaining) + " Until Deathmatch");
            }
            updateBoard("§6In-Game", formatTime(timeRemaining), GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
            if(timeRemaining == 600) {
                Bukkit.broadcastMessage(getFormat("formats.chests-refilled", true));
                World w = Bukkit.getWorld(MapManager.map);
                for(Chunk c : w.getLoadedChunks()) {
                    for(BlockState b : c.getTileEntities()) {
                        if(b instanceof Chest) {
                            Inventory inv = ((Chest) b).getBlockInventory();
                            fill(inv);
                        }
                    }
                }
            }
        }
    }
    @Override
    public void countdownComplete(Integer maxSeconds, Countdown countdown) {
        Deathmatch.startDeathmatch();
        GameState.setState(GameState.DEATHMATCH);
    }

}
