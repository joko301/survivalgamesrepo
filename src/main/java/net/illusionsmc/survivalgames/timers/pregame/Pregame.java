package net.illusionsmc.survivalgames.timers.pregame;

import com.portalmc.engine.utils.countdowns.Countdown;
import com.portalmc.engine.utils.countdowns.CountdownHandler;
import lombok.Data;
import lombok.Getter;
import me.confuser.barapi.BarAPI;
import net.illusionsmc.survivalgames.timers.endgame.Endgame;
import net.illusionsmc.survivalgames.timers.game.Game;
import net.illusionsmc.survivalgames.utils.GamePlayer;
import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.MapManager;
import net.illusionsmc.survivalgames.utils.Spectator;
import org.bukkit.*;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.Random;

import static com.portalmc.engine.utils.Formatter.formatTime;
import static net.illusionsmc.survivalgames.utils.ChestFiller.fill;
import static net.illusionsmc.survivalgames.utils.Messages.getFormat;
import static net.illusionsmc.survivalgames.utils.ScoreboardManager.updateBoard;

/**
 * Created by Joey on 7/25/2014.
 */
@Data
public final class Pregame implements CountdownHandler {

    @Getter private static Pregame instance;

    public static void startPregame() {
        Countdown cd = new Countdown(new Pregame(Pregame.getInstance()), 30);
        cd.startCoundown();
    }
    public static void stopPregame() {
        Countdown cd = new Countdown(new Pregame(Pregame.getInstance()), 30);
        cd.pauseCountdown();
    }

    private final Pregame pregameCD;
    @Override
    public void countdownStarting(Integer maxSeconds, Countdown countdown) {
        for(Player pl : Bukkit.getOnlinePlayers()) {
            BarAPI.setMessage(pl, "§cPregame §8| §630 seconds");
        }
        updateBoard("§cPregame", "30 seconds", GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
    }
    @Override
    public void countdownChanged(Integer maxSeconds, Integer timeRemaining, Countdown countdown) {
        for(Player pl : Bukkit.getOnlinePlayers()) {
            BarAPI.setMessage(pl, "§cPregame §8| §6" + formatTime(timeRemaining));
        }
        updateBoard("§cPregame", formatTime(timeRemaining), GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
    }
    @Override
    public void countdownComplete(Integer maxSeconds, Countdown countdown) {
        if(Bukkit.getOnlinePlayers().size() <= 1) {
            GameState.setState(GameState.ENDGAME);
            Endgame.startEndgame();
            Player p = GamePlayer.players.get(new Random().nextInt(GamePlayer.getPlayersSize()));
            Bukkit.broadcastMessage(getFormat("formats.game-won", true, new String[] {"<player>", p.getName()}));
        }
        for(Player pl : Bukkit.getOnlinePlayers()) {
            pl.playSound(pl.getLocation(), Sound.LEVEL_UP, 4, 1);
            pl.getInventory().clear();
            pl.getInventory().setArmorContents(null);
        }
        Game.startGame();
        GameState.setState(GameState.INGAME);
        World w = Bukkit.getWorld(MapManager.map);
        for(Chunk c : w.getLoadedChunks()) {
            for(BlockState b : c.getTileEntities()) {
                if(b instanceof Chest) {
                    Inventory inv = ((Chest) b).getBlockInventory();
                    fill(inv);
                }
            }
        }
    }

}
