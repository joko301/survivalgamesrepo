package net.illusionsmc.survivalgames.timers.endgame;

import com.portalmc.engine.utils.countdowns.Countdown;
import com.portalmc.engine.utils.countdowns.CountdownHandler;
import lombok.Data;
import lombok.Getter;
import me.confuser.barapi.BarAPI;
import net.illusionsmc.survivalgames.SurvivalGames;
import net.illusionsmc.survivalgames.utils.*;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Iterator;
import java.util.List;

import static com.portalmc.engine.utils.Formatter.formatTime;
import static net.illusionsmc.survivalgames.utils.ScoreboardManager.updateBoard;

/**
 * Created by Joey on 7/25/2014.
 */
@Data
public final class Endgame implements CountdownHandler {

    @Getter
    private static Endgame instance;

    public static void startEndgame() {
        Countdown cd = new Countdown(new Endgame(Endgame.getInstance()), 10);
        cd.startCoundown();
    }

    private final Endgame endgameCD;
    @Override
    public void countdownStarting(Integer maxSeconds, Countdown countdown) {
        World w = Bukkit.getWorld(MapManager.map);
        List<Entity> list = w.getEntities();
        Iterator<Entity> entities = list.iterator();
        while (entities.hasNext()) {
            Entity entity = entities.next();
            if (entity instanceof Item) {
                entity.remove();
            }
        }
        for(Player pl : Bukkit.getOnlinePlayers()) {
            BarAPI.setMessage(pl, "§cEndgame §8| §610 seconds");
        }
        updateBoard("§cEndgame", "10 seconds", GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
    }
    @Override
    public void countdownChanged(Integer maxSeconds, Integer timeRemaining, Countdown countdown) {
        for(Player pl : Bukkit.getOnlinePlayers()) {
            BarAPI.setMessage(pl, "§cEndgame §8| §6" + formatTime(timeRemaining));
            updateBoard("§cEndgame", formatTime(timeRemaining), GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
            if(timeRemaining == 1) {
                try{
                    ByteArrayOutputStream b = new ByteArrayOutputStream();
                    DataOutputStream out = new DataOutputStream(b);

                    out.writeUTF("Connect");
                    out.writeUTF("Hub");

                    pl.sendPluginMessage(SurvivalGames.getInstance(), "BungeeCord", b.toByteArray());
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @Override
    public void countdownComplete(Integer maxSeconds, Countdown countdown) {
        Bukkit.shutdown();
    }

}
