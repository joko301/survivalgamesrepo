package net.illusionsmc.survivalgames.timers.lobby;

import com.portalmc.engine.utils.countdowns.Countdown;
import com.portalmc.engine.utils.countdowns.CountdownHandler;
import lombok.Data;
import lombok.Getter;
import me.confuser.barapi.BarAPI;
import net.illusionsmc.survivalgames.SurvivalGames;
import net.illusionsmc.survivalgames.arena.SpawnLocations;
import net.illusionsmc.survivalgames.utils.*;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

import static com.portalmc.engine.utils.Formatter.formatTime;
import static net.illusionsmc.survivalgames.timers.pregame.Pregame.startPregame;
import static net.illusionsmc.survivalgames.utils.Messages.getFormat;
import static net.illusionsmc.survivalgames.utils.ScoreboardManager.updateBoard;

/**
 * Created by Joey on 7/24/2014.
 */
@Data
public final class Lobby implements CountdownHandler {

    @Getter private static Lobby instance;
    private static File locations;
    private static FileConfiguration configuration;

    public static void startLobby() {
        Countdown cd = new Countdown(new Lobby(Lobby.getInstance()), 60);
        cd.startCoundown();
    }

    private final Lobby lobbyCD;
    @Override
    public void countdownStarting(Integer maxSeconds, Countdown countdown) {
        for(Player pl : Bukkit.getOnlinePlayers()) {
            BarAPI.setMessage(pl, "§eLobby §8| §61:00 minutes");
        }
        updateBoard("§eLobby", "1:00 minutes", GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
    }
    @Override
    public void countdownChanged(Integer maxSeconds, Integer timeRemaining, Countdown countdown) {
        for(Player pl : Bukkit.getOnlinePlayers()) {
            BarAPI.setMessage(pl, "§eLobby §8| §6" + formatTime(timeRemaining));
        }
        updateBoard("§eLobby", formatTime(timeRemaining), GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
    }
    @Override
    public void countdownComplete(Integer maxSeconds, Countdown countdown) {
        locations = new File(SurvivalGames.getInstance().getDataFolder(), "locations.yml");
        configuration = YamlConfiguration.loadConfiguration(locations);
        if (Bukkit.getOnlinePlayers().size() <= 1) {
            for(Player pl : Bukkit.getOnlinePlayers()) {
                pl.playSound(pl.getLocation(), Sound.NOTE_BASS, 4, 1);
            }
            Bukkit.broadcastMessage(getFormat("formats.not-enough-players", true));
            startLobby();
        } else {
            for (Player pl : Bukkit.getOnlinePlayers()) {
                BarAPI.setMessage(pl, "§ePregame beginning...");
                pl.playSound(pl.getLocation(), Sound.LEVEL_UP, 4, 1);
            }
            updateBoard("§eLobby", "Starting...", GamePlayer.getPlayersSize(), Spectator.getSpectatorsSize());
            VoteManager.getVotedMap();
            SpawnLocations.teleportPlayersToMap(MapManager.map);
            GameState.setState(GameState.PREGAME);
            startPregame();
            Bukkit.broadcastMessage("§7§l§m---------------------------------------------");
            Bukkit.broadcastMessage("§6§oNow Playing Map: §e" + configuration.getString(MapManager.map + ".name"));
            Bukkit.broadcastMessage("§6§oMap Author: §e" + configuration .getString(MapManager.map + ".author"));
            Bukkit.broadcastMessage("§7§l§m---------------------------------------------");
        }
    }
}
