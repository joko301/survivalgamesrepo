package net.illusionsmc.survivalgames.utils;

import net.illusionsmc.survivalgames.SurvivalGames;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Joey on 8/17/2014.
 */
public class AfterlifeManager {

    public static Player target;
    private static List<String> afterlifePlayers = new ArrayList<>();
    private static File locations;
    private static FileConfiguration configuration;

    public static void setTarget(final Player p) {
        new BukkitRunnable() {
            public void run() {
                p.setCompassTarget(p.getKiller().getLocation());
            }
        }.runTaskTimer(SurvivalGames.getInstance(), 0, 2);
        target = p.getKiller();
    }
    public static void removeTarget(Player p) {
        p.setCompassTarget(null);
    }
    public static String getTarget() {
        return target.getName();
    }
    public static void setInAfterlife(String playerName) {
        Player p = Bukkit.getPlayer(playerName);
        p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1000000000, 1));
        afterlifePlayers.add(playerName);
    }
    public static void removeFromAfterlife(String playerName) {
        Player p = Bukkit.getPlayer(playerName);
        p.removePotionEffect(PotionEffectType.SPEED);
        if(afterlifePlayers.contains(playerName)) {
            afterlifePlayers.remove(playerName);
        }
    }
    public static void respawnPlayer(Player p) {
        List<Integer> in = new ArrayList<>();
        in.add(1);
        in.add(6);
        in.add(12);
        in.add(18);
        in.add(24);
        Integer i = in.get(new Random().nextInt(in.size()));

        locations = new File(SurvivalGames.getInstance().getDataFolder(), "locations.yml");
        configuration = YamlConfiguration.loadConfiguration(locations);
        World w = Bukkit.getWorld(configuration.getString(MapManager.map + ".world"));
        double x = configuration.getDouble(MapManager.map + ".spawn" + i + ".x");
        double y = configuration.getDouble(MapManager.map + ".spawn" + i + ".y");
        double z = configuration.getDouble(MapManager.map + ".spawn" + i + ".z");
        float pitch = configuration.getInt(MapManager.map + ".spawn" + i + ".pitch");
        float yaw = configuration.getInt(MapManager.map + ".spawn" + i + ".yaw");
        Location spawn = new Location(w, x, y, z, yaw, pitch);
        p.teleport(spawn);
    }

}
