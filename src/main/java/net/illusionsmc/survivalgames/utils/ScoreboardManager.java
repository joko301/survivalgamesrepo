package net.illusionsmc.survivalgames.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

/**
 * Created by Joey on 8/11/2014.
 */
public class ScoreboardManager {

    public static void updateBoard(String gameState, String timeLeft, Integer playersRemaining, Integer playersWatching) {
        org.bukkit.scoreboard.ScoreboardManager sb = Bukkit.getScoreboardManager();
        Scoreboard s = sb.getNewScoreboard();
        Objective o = s.registerNewObjective("survivalgames", "dummy");
        o.setDisplaySlot(DisplaySlot.SIDEBAR);
        o.setDisplayName("§6iSG §8- §r" + gameState);
        o.getScore("§eTime Left§8:").setScore(7);
        o.getScore("§6" + timeLeft).setScore(6);
        o.getScore(" ").setScore(5);
        o.getScore("§eTributes§8:").setScore(4);
        o.getScore("§6" + playersRemaining).setScore(3);
        o.getScore("§eWatching§8:").setScore(2);
        o.getScore("§6" + playersWatching).setScore(1);
        for(Player pl : Bukkit.getOnlinePlayers()) {
            pl.setScoreboard(s);
        }
    }

}
