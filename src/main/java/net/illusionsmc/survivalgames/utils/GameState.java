package net.illusionsmc.survivalgames.utils;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Joey on 7/24/2014.
 */
public enum GameState {

    LOBBY, PREGAME, INGAME, ENDGAME, DEATHMATCH;

    @Getter private static GameState state;

    public static void setState(GameState state) {
        GameState.state = state;
    }
}
