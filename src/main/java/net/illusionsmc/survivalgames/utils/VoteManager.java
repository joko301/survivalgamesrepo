package net.illusionsmc.survivalgames.utils;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joey on 8/13/2014.
 */
public class VoteManager {

    public static List<String> hasVoted = new ArrayList<>();
    public static List<Integer> cloudnine = new ArrayList<>();
    public static List<Integer> avarcia = new ArrayList<>();

    public static List<Integer> mapVotes = new ArrayList<>();

    public static Boolean canVote(Player p) {
        if(hasVoted.contains(p.getName())) {
            return false;
        }else{
            return true;
        }
    }
    public static void setCanVote(Player p, Boolean b) {
        if(b == true) {
            if(hasVoted.contains(p.getName())) {
                hasVoted.remove(p.getName());
            }
        }else if(b == false) {
            hasVoted.add(p.getName());
        }
    }
    public static void getVotedMap() {
        if((cloudnine.size() > avarcia.size())){
            MapManager.map = "cloudnine";
        }
        if((avarcia.size() > cloudnine.size())){
            MapManager.map = "avarcia";
        }
        if((cloudnine.size() == avarcia.size())){
            MapManager.map = "cloudnine";
        }
    }
    public static Integer getVotes(String mapName) {
        Integer votes = null;
        if(mapName.equalsIgnoreCase("cloudnine")) {
            votes = cloudnine.size();
        }else if(mapName.equalsIgnoreCase("avarcia")) {
            votes = avarcia.size();
        }
        return votes;
    }
    public static void addVote(List<Integer> al) {
        al.add(1);
    }

}
