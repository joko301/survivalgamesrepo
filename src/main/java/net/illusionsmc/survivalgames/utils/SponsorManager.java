package net.illusionsmc.survivalgames.utils;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joey on 8/13/2014.
 */
public class SponsorManager {
    private static List<String> playersSponsorAble = new ArrayList<>();

    public static Boolean isSponsorAble(Player p) {
        if(playersSponsorAble.contains(p.getName())) {
            return true;
        }else{
            return false;
        }
    }
    public static void setSponsorAble(Player p, Boolean b) {
        if(b == true) {
            playersSponsorAble.add(p.getName());
        }else if(b == false) {
            if(playersSponsorAble.contains(p.getName())) {
                playersSponsorAble.remove(p.getName());
            }
        }
    }

}
