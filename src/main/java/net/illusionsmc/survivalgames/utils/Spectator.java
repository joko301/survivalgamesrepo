package net.illusionsmc.survivalgames.utils;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joey on 7/26/2014.
 */
public class Spectator {

    public static List<Player> spectators = new ArrayList<>();

    public static void addSpectator(Player p) {
        spectators.add(p);
    }
    public static void removeSpectator(Player p) {
        spectators.remove(p);
    }
    public static Integer getSpectatorsSize() {
        return spectators.size();
    }

}
