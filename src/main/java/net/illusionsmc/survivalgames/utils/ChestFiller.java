package net.illusionsmc.survivalgames.utils;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Joey on 7/24/2014.
 */
public final class ChestFiller {

    public static void fill(Inventory inv) {
        inv.clear();
        Random random = new Random();
        List<ItemStack> items = new ArrayList<>();
        //Misc, useable but not needed
        items.add(new ItemStack(Material.BOAT));
        items.add(new ItemStack(Material.FISHING_ROD));
        items.add(new ItemStack(Material.DIAMOND));
        items.add(new ItemStack(Material.IRON_INGOT));
        items.add(new ItemStack(Material.GOLD_INGOT));
        items.add(new ItemStack(Material.STICK));

        //Weapons
        items.add(new ItemStack(Material.FLINT_AND_STEEL));
        items.add(new ItemStack(Material.WOOD_AXE));
        items.add(new ItemStack(Material.WOOD_SWORD));
        items.add(new ItemStack(Material.STONE_AXE));
        items.add(new ItemStack(Material.STONE_SWORD));
        items.add(new ItemStack(Material.BOW));
        items.add(new ItemStack(Material.ARROW, 5));

        //Armor
        items.add(new ItemStack(Material.LEATHER_BOOTS));
        items.add(new ItemStack(Material.LEATHER_LEGGINGS));
        items.add(new ItemStack(Material.LEATHER_CHESTPLATE));
        items.add(new ItemStack(Material.LEATHER_HELMET));
        items.add(new ItemStack(Material.CHAINMAIL_BOOTS));
        items.add(new ItemStack(Material.CHAINMAIL_LEGGINGS));
        items.add(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
        items.add(new ItemStack(Material.CHAINMAIL_HELMET));
        items.add(new ItemStack(Material.GOLD_BOOTS));
        items.add(new ItemStack(Material.GOLD_LEGGINGS));
        items.add(new ItemStack(Material.GOLD_CHESTPLATE));
        items.add(new ItemStack(Material.GOLD_HELMET));
        items.add(new ItemStack(Material.IRON_BOOTS));
        items.add(new ItemStack(Material.IRON_LEGGINGS));
        items.add(new ItemStack(Material.IRON_CHESTPLATE));
        items.add(new ItemStack(Material.IRON_HELMET));

        //Food
        items.add(new ItemStack(Material.APPLE));
        items.add(new ItemStack(Material.GOLDEN_APPLE));
        items.add(new ItemStack(Material.GOLDEN_CARROT));
        items.add(new ItemStack(Material.BREAD));
        items.add(new ItemStack(Material.PORK));
        items.add(new ItemStack(Material.GRILLED_PORK));
        items.add(new ItemStack(Material.RAW_FISH));
        items.add(new ItemStack(Material.COOKED_FISH));
        items.add(new ItemStack(Material.COOKIE));
        items.add(new ItemStack(Material.MELON));
        items.add(new ItemStack(Material.RAW_BEEF));
        items.add(new ItemStack(Material.COOKED_BEEF));
        items.add(new ItemStack(Material.COOKED_CHICKEN));
        items.add(new ItemStack(Material.RAW_CHICKEN));
        items.add(new ItemStack(Material.CARROT_ITEM));
        items.add(new ItemStack(Material.POTATO_ITEM));
        items.add(new ItemStack(Material.BAKED_POTATO));
        for (int i = 0; i < 4; i++) {
            ItemStack item = (ItemStack) items.get(random.nextInt(items.size()));
            inv.setItem(random.nextInt(inv.getSize() - 1), item);
        }
    }

}
