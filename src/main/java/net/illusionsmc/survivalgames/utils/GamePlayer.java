package net.illusionsmc.survivalgames.utils;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joey on 7/25/2014.
 */
public class GamePlayer {

    public static List<Player> players = new ArrayList<>();

    public static void addPlayer(Player p) {
        players.add(p);
    }
    public static void removePlayer(Player p) {
        players.remove(p);
    }
    public static Integer getPlayersSize() {
        return players.size();
    }

}
