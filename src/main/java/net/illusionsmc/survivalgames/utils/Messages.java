package net.illusionsmc.survivalgames.utils;

import net.illusionsmc.survivalgames.SurvivalGames;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Created by Joey on 7/24/2014.
 */
public final class Messages {

    public static String getFormat(String s) {
        return getFormat(SurvivalGames.getInstance().getConfig(), s, false);
    }

    public static String getFormat(String s, Boolean b, String[]... changes) {
        return getFormat(SurvivalGames.getInstance().getConfig(), s, b, changes);
    }

    public static String getFormat(ConfigurationSection configurationSection, String s, boolean b, String[]... changes) {
        String format = (b ? getFormat(configurationSection, "prefix", false)+" " : "")+ ChatColor.translateAlternateColorCodes('&', configurationSection.getString(s, s));
        for(String[] ch: changes) {
            if(ch.length == 2) format = format.replace(ch[0], ch[1]);
        }
        return format;
    }

}
