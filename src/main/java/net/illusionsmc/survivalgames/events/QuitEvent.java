package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.utils.GamePlayer;
import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.Spectator;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 7/25/2014.
 */
public class QuitEvent implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (GamePlayer.players.contains(p)) {
            GamePlayer.removePlayer(p);
        } else if (Spectator.spectators.contains(p)) {
            Spectator.removeSpectator(p);
            p.setAllowFlight(false);
            p.setFlying(false);
        }
        e.setQuitMessage(getFormat("formats.player-quit-format", true, new String[] {"<player>", p.getDisplayName()}, new String[] {"<online>", Bukkit.getOnlinePlayers().size()-1 + ""}, new String[] {"<max>", Bukkit.getMaxPlayers() + ""}));
    }

}
