package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.Spectator;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

/**
 * Created by Joey on 7/27/2014.
 */
public class ItemUtils implements Listener {

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        Player p = e.getPlayer();
        if(GameState.getState() == GameState.LOBBY || GameState.getState() == GameState.PREGAME || GameState.getState() == GameState.ENDGAME || Spectator.spectators.contains(p)) {
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void onPickup(PlayerPickupItemEvent e) {
        Player p = e.getPlayer();
        if(GameState.getState() == GameState.LOBBY || GameState.getState() == GameState.PREGAME || GameState.getState() == GameState.ENDGAME || Spectator.spectators.contains(p)) {
            e.setCancelled(true);
        }
    }

}
