package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by Joey on 7/25/2014.
 */
public class PlayerMove implements Listener {

    @EventHandler
    public void onFrozenMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if(GameState.getState() == GameState.PREGAME) {
            if(e.getTo().getX() == e.getFrom().getX() && e.getTo().getY() == e.getFrom().getY() && e.getTo().getZ() == e.getFrom().getZ()) {
                return;
            }
            Location back = new Location(e.getFrom().getWorld(), e.getFrom().getX(), e.getFrom().getY(), e.getFrom().getZ(), e.getFrom().getYaw(), e.getFrom().getPitch());
            p.teleport(back);
        }
        if(GameState.getState() == GameState.DEATHMATCH) {
            World w = Bukkit.getWorld(MapManager.map);
            if(e.getTo().getX() > w.getSpawnLocation().getX()+35 || e.getTo().getX() < w.getSpawnLocation().getX()-35 || e.getTo().getZ() > w.getSpawnLocation().getZ()+35 || e.getTo().getZ() < w.getSpawnLocation().getZ()-35) {
                e.setTo(e.getFrom());
            }
        }
    }

}
