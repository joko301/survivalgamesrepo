package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.utils.GamePlayer;
import net.illusionsmc.survivalgames.utils.GameState;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * Created by Joey on 7/25/2014.
 */
public class LoginEvent implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        if(GameState.getState() == GameState.PREGAME || GameState.getState() == GameState.DEATHMATCH || GameState.getState() == GameState.ENDGAME) {
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, ChatColor.RED + "You cannot join during this state of game!");
        }
        if(GamePlayer.getPlayersSize() == 24) {
            e.disallow(PlayerLoginEvent.Result.KICK_FULL, ChatColor.RED + "This server is full!");
        }
    }

}
