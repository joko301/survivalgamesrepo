package net.illusionsmc.survivalgames.events;

import me.confuser.barapi.BarAPI;
import net.illusionsmc.survivalgames.SurvivalGames;
import net.illusionsmc.survivalgames.statsmanager.GamesWon;
import net.illusionsmc.survivalgames.statsmanager.PlayersKilled;
import net.illusionsmc.survivalgames.statsmanager.Points;
import net.illusionsmc.survivalgames.timers.endgame.Endgame;
import net.illusionsmc.survivalgames.timers.game.Game;
import net.illusionsmc.survivalgames.utils.GamePlayer;
import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.Spectator;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static net.illusionsmc.survivalgames.SurvivalGames.createItem;
import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 7/25/2014.
 */
public class DeathEvent implements Listener {

    private static ItemStack playerSpec;

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        final Player p = e.getEntity();
        Integer remaining = GamePlayer.getPlayersSize()-1;
        GamePlayer.removePlayer(p);
        e.setDeathMessage(getFormat("formats.tribute-fallen-message", true, new String[] {"<player>", p.getDisplayName()}, new String[] {"<amount>", GamePlayer.getPlayersSize() + ""}));
        Spectator.addSpectator(p);
        p.sendMessage(getFormat("formats.new-spectator", true));
        if(Points.getPoints(p.getName()) >= 5) {
            p.sendMessage(getFormat("formats.points-lost-death", true, new String[] {"<points>", "5"}));
            Points.removePoints(p, 5);
        }
        if(e.getEntity().getKiller() instanceof Player) {
            e.getEntity().getKiller().sendMessage(getFormat("formats.points-received", true, new String[] {"<points>", "5"}, new String[] {"<reason>", "killing " + p.getDisplayName() + "!"}));
            Points.addPoints(e.getEntity().getKiller(), 5);
            PlayersKilled.addPlayersKilled(e.getEntity().getKiller(), 1);
        }
        if(p.isDead()) {
            p.setHealth(20);
            Bukkit.getScheduler().scheduleSyncDelayedTask(SurvivalGames.getInstance(), new Runnable() {
                @Override
                public void run() {
                    playerSpec = createItem(Material.COMPASS, "§eSpectator", "§8Easier then a teleport.");
                    p.getInventory().setItem(0, playerSpec);
                    p.setHealth(20);
                    p.setFoodLevel(20);
                }
            }, 5);
        }

        for(Player pl : Bukkit.getOnlinePlayers()) {
            pl.hidePlayer(p);
            if(Spectator.spectators.contains(pl)) {
                p.hidePlayer(pl);
            }
        }
        p.setAllowFlight(true);
        p.setFlying(true);
        if(GamePlayer.getPlayersSize() <= 1) {
            Player pl = GamePlayer.players.get(new Random().nextInt(GamePlayer.getPlayersSize()));
            Points.addPoints(pl, 20);
            GamesWon.addGamesWon(pl, 1);
            pl.sendMessage(getFormat("formats.points-received", true, new String[] {"<points>", "20"}, new String[] {"<reason>", "winning the Survival Games!"}));
            Bukkit.broadcastMessage(getFormat("formats.game-won", true, new String[] {"<player>", pl.getDisplayName()}));
            for(Player ply : Bukkit.getOnlinePlayers()) {
                ply.getInventory().clear();
                ply.getInventory().setArmorContents(null);
            }
        }
    }

}
