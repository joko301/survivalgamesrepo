package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.utils.GamePlayer;
import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.Spectator;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

/**
 * Created by Joey on 7/26/2014.
 */
public class PlayerUtils implements Listener {

    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent e) {
        Player p = (Player)e.getEntity();
        if(GameState.getState() == GameState.LOBBY || GameState.getState() == GameState.PREGAME || GameState.getState() == GameState.ENDGAME || Spectator.spectators.contains(p)) {
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void onDamageTaken(EntityDamageEvent e) {
        Player p = (Player)e.getEntity();
        if(e.getEntity() instanceof Player) {
            if(GameState.getState() == GameState.LOBBY || GameState.getState() == GameState.PREGAME || GameState.getState() == GameState.ENDGAME || Spectator.spectators.contains(p)) {
                e.setCancelled(true);
            }
        }
    }
    @EventHandler
    public void onPVPDamageTaken(EntityDamageByEntityEvent e) {
        Player p = (Player)e.getEntity();
        Player dmgr = (Player)e.getDamager();
        if(GameState.getState() == GameState.LOBBY || GameState.getState() == GameState.PREGAME || GameState.getState() == GameState.ENDGAME) {
            e.setCancelled(true);
        }
        if(Spectator.spectators.contains(dmgr)) {
            e.setCancelled(true);
        }
    }

}
