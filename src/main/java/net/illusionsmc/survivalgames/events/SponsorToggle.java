package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.utils.SponsorManager;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Map;

import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 8/12/2014.
 */
public class SponsorToggle implements Listener {

    private static ItemStack sponsorsOn, sponsorsOff;
    private final int cdtime = 3;
    private Map<String, Long> lastUsage = new HashMap<String, Long>();

    @EventHandler
    public void onToggle(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if(p.getItemInHand().getType().equals(Material.STAINED_GLASS)) {
                sponsorsOn = new ItemStack(Material.STAINED_GLASS, 1, DyeColor.LIME.getData());
                ItemMeta sponsorsOnMeta = sponsorsOn.getItemMeta();
                sponsorsOnMeta.setDisplayName("§eSponsors §8| §aOn");
                sponsorsOn.setItemMeta(sponsorsOnMeta);
                sponsorsOff = new ItemStack(Material.STAINED_GLASS, 1, DyeColor.RED.getData());
                ItemMeta sponsorsOffMeta = sponsorsOff.getItemMeta();
                sponsorsOffMeta.setDisplayName("§eSponsors §8| §cOff");
                sponsorsOff.setItemMeta(sponsorsOffMeta);
                long lastUsed = 0;
                if (lastUsage.containsKey(p.getName())) {
                    lastUsed = lastUsage.get(p.getName());
                }
                int cdmillis = cdtime * 1000;
                if (System.currentTimeMillis() - lastUsed >= cdmillis) {
                    if (p.getItemInHand().getItemMeta().equals(sponsorsOnMeta)) {
                        p.getInventory().setItemInHand(sponsorsOff);
                        SponsorManager.setSponsorAble(p, false);
                        p.sendMessage(getFormat("formats.sponsor-toggle-message", true, new String[] {"<mode>", "off"}));
                        p.playSound(p.getLocation(), Sound.NOTE_BASS, 4, 1);
                    } else if (p.getItemInHand().getItemMeta().equals(sponsorsOffMeta)) {
                        p.getInventory().setItemInHand(sponsorsOn);
                        SponsorManager.setSponsorAble(p, true);
                        p.sendMessage(getFormat("formats.sponsor-toggle-message", true, new String[] {"<mode>", "on"}));
                        p.playSound(p.getLocation(), Sound.NOTE_PIANO, 4, 1);
                    }
                    lastUsage.put(p.getName(), System.currentTimeMillis());
                } else {
                    int timeLeft = (int) (cdtime - ((System.currentTimeMillis() - lastUsed) / 1000));
                    p.sendMessage(getFormat("formats.item-cooldown", true));
                }
            }
        }
    }

}
