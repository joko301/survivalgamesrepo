package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.statsmanager.Points;
import net.illusionsmc.survivalgames.utils.Spectator;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 7/31/2014.
 */
public class ChatHandler implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if(Spectator.spectators.contains(e.getPlayer())) {
            try{
                e.setCancelled(true);
                e.getPlayer().sendMessage(getFormat("formats.spectator-chat", true));
            }catch (Exception ex) {
                e.setCancelled(true);
                e.getPlayer().sendMessage(getFormat("formats.spectator-chat", true));
            }
        }
    }
}
