package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.SurvivalGames;
import net.illusionsmc.survivalgames.arena.SpawnLocations;
import net.illusionsmc.survivalgames.utils.*;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.ArrayList;

import static com.portalmc.engine.gamehandler.BookHandler.giveBook;
import static net.illusionsmc.survivalgames.SurvivalGames.createItem;
import static net.illusionsmc.survivalgames.SurvivalGames.createStainedGlass;
import static net.illusionsmc.survivalgames.utils.Messages.getFormat;
import static net.illusionsmc.survivalgames.utils.ScoreboardManager.updateBoard;

/**
 * Created by Joey on 7/25/2014.
 */
public class JoinEvent implements Listener {

    private static File locations, stats;
    private static FileConfiguration configuration, statsconfig;
    private static ItemStack sponsorToggle, mapvoter;

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        locations = new File(SurvivalGames.getInstance().getDataFolder(), "locations.yml");
        configuration = YamlConfiguration.loadConfiguration(locations);
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        Player p = e.getPlayer();
        if(GameState.getState() == GameState.INGAME) {
            p.sendMessage(getFormat("formats.joined-new-spectator", true));
            for(Player pl : Bukkit.getOnlinePlayers()) {
                pl.hidePlayer(p);
                if(Spectator.spectators.contains(pl)) {
                    p.hidePlayer(pl);
                }
            }
            if(configuration.getConfigurationSection("lobby") == null) {
                p.sendMessage(getFormat("formats.lobby-not-set", true));
            }else{
                World w = Bukkit.getWorld((String)configuration.get("lobby.world"));
                double x = configuration.getDouble("lobby.x");
                double y = configuration.getDouble("lobby.y");
                double z = configuration.getDouble("lobby.z");
                float yaw = configuration.getInt("lobby.yaw");
                float pitch = configuration.getInt("lobby.pitch");
                Location lobby = new Location(w, x, y, z, yaw, pitch);
                p.teleport(lobby);
            }
            Spectator.addSpectator(p);
            p.setAllowFlight(true);
            p.setFlying(true);
            p.setFoodLevel(20);
            p.setSaturation(8F);
            p.setHealth(20);
            p.getInventory().clear();
            p.getInventory().setArmorContents(null);
            ArrayList<String> pages = new ArrayList<>();
            pages.add(ChatColor.UNDERLINE + "Welcome to the Survival Games! \n The reason you clicked this book was because you wanted to learn how to play? \n Alright! Well, let's start off explaining Survival Games. Survival Games is a Minecraft based mini-game off of the Hunger Games movies/books.");
            pages.add(ChatColor.UNDERLINE + "Teaming \n Teaming is allowed, but please keep the amount of players on your team below 3-4 to keep the annoyance of other players low. We want everyone to have fun whilst playing!");
            pages.add(ChatColor.UNDERLINE + "Fighting \n Fighting anyone is allowed, whether they kindly ask you to stop or not! The last person standing wins, so have fun!");
            pages.add(ChatColor.UNDERLINE + "Loot \n In order to get loot, you have to find chests. They will be filled with all kinds of goodies for surviving the slaughter.");
            giveBook(p, "ItsJoko", ChatColor.YELLOW + "How to Play", pages, 8);
            ItemStack nav = createItem(Material.COMPASS, ChatColor.YELLOW + "Player Spectator", ChatColor.DARK_GRAY + "Easier than a teleport.");
            p.getInventory().setItem(0, nav);
            p.sendMessage(getFormat("formats.server-beta-message", false));
        }
        else if(GameState.getState() == GameState.LOBBY) {
            if(configuration.getConfigurationSection("lobby") == null) {
                p.sendMessage(getFormat("formats.lobby-not-set", true));
            }else{
                World w = Bukkit.getWorld((String)configuration.get("lobby.world"));
                double x = configuration.getDouble("lobby.x");
                double y = configuration.getDouble("lobby.y");
                double z = configuration.getDouble("lobby.z");
                float yaw = configuration.getInt("lobby.yaw");
                float pitch = configuration.getInt("lobby.pitch");
                Location lobby = new Location(w, x, y, z, yaw, pitch);
                p.teleport(lobby);
            }
            GamePlayer.addPlayer(p);
            p.setFoodLevel(20);
            p.setSaturation(8F);
            p.setHealth(20);
            p.getInventory().clear();
            p.getInventory().setArmorContents(null);
            p.setGameMode(GameMode.SURVIVAL);
            ArrayList<String> pages = new ArrayList<>();
            pages.add("Welcome to the Survival Games!\n The reason you clicked this book was because you wanted to learn how to play? Alright! Well, let's start off explaining Survival Games. Survival Games is a Minecraft based mini-game off of the Hunger Games movies/books.");
            pages.add("Teaming \n Teaming is allowed, but please keep the amount of players on your team below 3-4 to keep the annoyance of other players low. We want everyone to have fun whilst playing!");
            pages.add("Fighting \n Fighting anyone is allowed, whether they kindly ask you to stop or not! The last person standing wins, so have fun!");
            pages.add("Loot \n In order to get loot, you have to find chests. They will be filled with all kinds of goodies for surviving the slaughter.");
            giveBook(p, "ItsJoko", ChatColor.YELLOW + "How to Play", pages, 8);
            mapvoter = createItem(Material.PAPER, "§eMap Voter", "§8Vote for a map!");
            sponsorToggle = new ItemStack(Material.STAINED_GLASS, 1, DyeColor.LIME.getData());
            ItemMeta meta = sponsorToggle.getItemMeta();
            meta.setDisplayName("§eSponsors §8| §aOn");
            sponsorToggle.setItemMeta(meta);
            p.getInventory().setItem(7, sponsorToggle);
            p.getInventory().setItem(0, mapvoter);
            p.sendMessage(getFormat("formats.server-beta-message", false));
            SponsorManager.setSponsorAble(p, true);
            VoteManager.setCanVote(p, true);
        }
        e.setJoinMessage(getFormat("formats.player-join-format", true, new String[] {"<player>", p.getName()}, new String[] {"<online>", Bukkit.getOnlinePlayers().size() + ""}, new String[] {"<max>", Bukkit.getMaxPlayers() + ""}));
        if(statsconfig.get(p.getName() + ".points") == null) {
            statsconfig.set(p.getName() + ".points", 100);
            try{
                statsconfig.save(stats);
            }catch(Exception ex) {
                ex.printStackTrace();
            }
        }
        if(statsconfig.get(p.getName() + ".gameswon") == null) {
            statsconfig.set(p.getName() + ".gameswon", 0);
            try{
                statsconfig.save(stats);
            }catch(Exception ex) {
                ex.printStackTrace();
            }
        }
        if(statsconfig.get(p.getName() + ".playerskilled") == null) {
            statsconfig.set(p.getName() + ".playerskilled", 0);
            try{
                statsconfig.save(stats);
            }catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

}
