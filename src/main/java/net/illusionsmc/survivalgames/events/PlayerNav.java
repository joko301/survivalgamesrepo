package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.SurvivalGames;
import net.illusionsmc.survivalgames.utils.GamePlayer;
import net.illusionsmc.survivalgames.utils.MapManager;
import net.illusionsmc.survivalgames.utils.Spectator;
import net.illusionsmc.survivalgames.utils.VoteManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 8/17/2014.
 */
public class PlayerNav implements Listener {

    private static Inventory playernav;

    @EventHandler
    public void onPlayerNavigatorClick(PlayerInteractEvent e) {
        if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if(e.getPlayer().getItemInHand().getType().equals(Material.COMPASS)) {
                if(Spectator.spectators.contains(e.getPlayer())) {
                    playernav = Bukkit.createInventory(null, 27, "Player Navigator");

                    Bukkit.getScheduler().scheduleSyncRepeatingTask(SurvivalGames.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i < GamePlayer.getPlayersSize(); i++) {
                                Player p = GamePlayer.players.get(i);
                                ItemStack item = new ItemStack(Material.SKULL_ITEM);
                                ItemMeta itemMeta = item.getItemMeta();
                                String playerName = p.getName();
                                itemMeta.setDisplayName(playerName);
                                itemMeta.setLore(Arrays.asList("§eHealth§8: §6" + p.getHealth(), "Left click to spectate."));
                                item.setItemMeta(itemMeta);
                                playernav.setItem(i, item);
                            }
                        }
                    }, 0, 2);
                    e.getPlayer().openInventory(playernav);
                    e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.CHEST_OPEN, 4, 1);

                }
            }
        }
    }
    @EventHandler
    public void playerNav(InventoryClickEvent e) {
        Player p = (Player)e.getWhoClicked();
        if(e.getInventory() == null) return;
        if (!e.getInventory().getName().equalsIgnoreCase(playernav.getName())) return;
        if (e.getCurrentItem().getItemMeta() == null) return;
        if(e.getCurrentItem().getType().equals(Material.SKULL_ITEM)) {
            Player t = Bukkit.getPlayer(e.getCurrentItem().getItemMeta().getDisplayName());
            e.setCancelled(true);
            p.closeInventory();
            p.teleport(t.getLocation());
        }
        if(e.getCurrentItem() == null) {
            return;
        }
    }

}
