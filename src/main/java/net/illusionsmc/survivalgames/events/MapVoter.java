package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.SurvivalGames;
import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.MapManager;
import net.illusionsmc.survivalgames.utils.VoteManager;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.Arrays;

import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 8/12/2014.
 */
public class MapVoter implements Listener {

    private static Inventory voter;
    private static File locations;
    private static FileConfiguration configuration;

    @EventHandler
    public void onMapVoterOpen(PlayerInteractEvent e) {
        if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if(e.getPlayer().getItemInHand().getType().equals(Material.PAPER)) {
                if (GameState.getState() == GameState.LOBBY) {
                    locations = new File(SurvivalGames.getInstance().getDataFolder(), "locations.yml");
                    configuration = YamlConfiguration.loadConfiguration(locations);
                    voter = Bukkit.createInventory(null, 9, "Map Voter §c[BETA]");
                    for (int i = 0; i < MapManager.mapName.size(); i++) {
                        ItemStack item = new ItemStack(Material.PAPER);
                        ItemMeta itemMeta = item.getItemMeta();
                        String mapName = configuration.getString(MapManager.mapName.get(i) + ".name");
                        String rawMapName = MapManager.mapName.get(i);
                        itemMeta.setDisplayName(ChatColor.YELLOW + mapName);
                        itemMeta.setLore(Arrays.asList(ChatColor.DARK_GRAY + "Votes: " + VoteManager.getVotes(MapManager.mapName.get(i))));
                        item.setItemMeta(itemMeta);
                        voter.setItem(i, item);
                    }
                    e.getPlayer().openInventory(voter);
                    e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.CHEST_OPEN, 4, 1);
                }else{
                    e.getPlayer().sendMessage(getFormat("formats.no-voting", true));
                }
            }
        }
    }
    @EventHandler
    public void vote(InventoryClickEvent e) {
        Player p = (Player)e.getWhoClicked();
        if(e.getInventory() == null) return;
        if (!e.getInventory().getName().equalsIgnoreCase(voter.getName())) return;
        if (e.getCurrentItem().getItemMeta() == null) return;
        if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Cloud Nine")) {
            if (VoteManager.canVote(p)) {
                e.setCancelled(true);
                p.closeInventory();
                VoteManager.addVote(VoteManager.cloudnine);
                VoteManager.setCanVote(p, false);
                p.sendMessage(getFormat("formats.voted-for-map", true, new String[] {"<map>", "Cloud Nine"}));
                p.playSound(p.getLocation(), Sound.LEVEL_UP, 4, 1);
            }else{
                p.sendMessage(getFormat("formats.already-voted", true));
                e.setCancelled(true);
                p.closeInventory();
            }
        }
        if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Avaricia")) {
            if (VoteManager.canVote(p)) {
                e.setCancelled(true);
                p.closeInventory();
                VoteManager.addVote(VoteManager.avarcia);
                VoteManager.setCanVote(p, false);
                p.sendMessage(getFormat("formats.voted-for-map", true, new String[] {"<map>", "Avaricia"}));
                p.playSound(p.getLocation(), Sound.LEVEL_UP, 4, 1);
            }else{
                p.sendMessage(getFormat("formats.already-voted", true));
                e.setCancelled(true);
                p.closeInventory();
            }
        }
        if(e.getCurrentItem() == null) {
            return;
        }
    }

}
