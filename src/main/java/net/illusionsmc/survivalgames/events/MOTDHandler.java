package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.utils.GameState;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

/**
 * Created by Joey on 7/25/2014.
 */
public class MOTDHandler implements Listener {

    @EventHandler
    public void onPing(ServerListPingEvent e) {
        if(GameState.getState() == GameState.LOBBY) {
            e.setMotd(ChatColor.YELLOW + "Lobby");
        }
        if(GameState.getState() == GameState.PREGAME) {
            e.setMotd(ChatColor.RED + "Pregame");
        }
        if(GameState.getState() == GameState.INGAME) {
            e.setMotd(ChatColor.GOLD + "In-Game");
        }
        if(GameState.getState() == GameState.DEATHMATCH) {
            e.setMotd(ChatColor.RED + "Deathmatch");
        }
        if(GameState.getState() == GameState.ENDGAME) {
            e.setMotd(ChatColor.RED + "Endgame");
        }
    }

}
