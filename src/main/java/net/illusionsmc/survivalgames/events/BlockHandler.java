package net.illusionsmc.survivalgames.events;

import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.Spectator;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Joey on 7/26/2014.
 */
public class BlockHandler implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if(GameState.getState() == GameState.INGAME || GameState.getState() == GameState.DEATHMATCH) {
            if (!(e.getBlock().getType().equals(Material.LONG_GRASS) || e.getBlock().getType().equals(Material.VINE) || e.getBlock().getType().equals(Material.LEAVES) || e.getBlock().getType().equals(Material.LEAVES_2) || e.getBlock().getType().equals(Material.WHEAT) )) {
                e.setCancelled(true);
            }
            if(Spectator.spectators.contains(e.getPlayer())) {
                e.setCancelled(true);
            }
        }else{
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        if(GameState.getState() == GameState.INGAME || GameState.getState() == GameState.DEATHMATCH || Spectator.spectators.contains(e.getPlayer())) {
            if (!(e.getBlock().getType().equals(Material.BOAT) || e.getBlock().getType().equals(Material.FIRE) || e.getBlock().getType().equals(Material.LEAVES) || e.getBlock().getType().equals(Material.LEAVES_2))) {
                e.setCancelled(true);
            }
            if(Spectator.spectators.contains(e.getPlayer())) {
                e.setCancelled(true);
            }
        }else{
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            BlockState b = e.getClickedBlock().getState();
            if(b instanceof Chest) {
                if(Spectator.spectators.contains(p)) {
                    e.setCancelled(true);
                }
            }
        }
    }
    @EventHandler
    public void onItemFrameBreak(HangingBreakByEntityEvent e) {
        if(e.getEntity() instanceof ItemFrame) {
            e.setCancelled(true);
        }
    }

}
