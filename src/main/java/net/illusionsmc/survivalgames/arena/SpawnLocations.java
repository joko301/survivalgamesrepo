package net.illusionsmc.survivalgames.arena;

import net.illusionsmc.survivalgames.SurvivalGames;
import net.illusionsmc.survivalgames.utils.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Joey on 7/25/2014.
 */
public class  SpawnLocations {

    private static File locations;
    private static FileConfiguration configuration;

    public static void teleportPlayersToMap(String mapName) {
        locations = new File(SurvivalGames.getInstance().getDataFolder(), "locations.yml");
        configuration = YamlConfiguration.loadConfiguration(locations);
        List<Integer> spawnpoints = new ArrayList<>();
        for(Integer i : configuration.getIntegerList(mapName)) {
            spawnpoints.add(i);
        }
        for(final Player pl : Bukkit.getOnlinePlayers()) {
            Integer i = spawnpoints.get(new Random().nextInt(spawnpoints.size()));
            World w = Bukkit.getWorld(configuration.getString(mapName + ".world"));
            Double x = configuration.getDouble(mapName + "." + i + ".x");
            Double y = configuration.getDouble(mapName + i + ".y");
            Double z = configuration.getDouble(mapName + i + ".z");
            float yaw = configuration.getInt(mapName + i + ".yaw");
            float pitch = configuration.getInt(mapName + i + ".pitch");
            spawnpoints.remove(i);
            final Location spawnpoint = new Location(w, x, y, z, yaw, pitch);
            Bukkit.getScheduler().scheduleSyncDelayedTask(SurvivalGames.getInstance(), new Runnable() {
                @Override
                public void run() {
                    pl.teleport(spawnpoint);
                }
            }, 10);
        }
    }

}
