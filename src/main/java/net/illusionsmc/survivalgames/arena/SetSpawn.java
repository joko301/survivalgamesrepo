package net.illusionsmc.survivalgames.arena;

import com.portalmc.engine.command.Permission;
import com.portalmc.engine.command.PortalCommandHandler;
import net.illusionsmc.survivalgames.SurvivalGames;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 7/25/2014.
 */
@Permission("survivalgames.setspawn")
public class SetSpawn extends PortalCommandHandler {

    private File locations;
    private FileConfiguration configuration;

    public SetSpawn() {
        super("setspawn");
    }
    @Override
    protected void playerExecutesCommand(Player p, String[] args) {
        if(args.length == 0) {
            p.sendMessage(getFormat("formats.specify-spawn-id", true));
        }
        locations = new File(SurvivalGames.getInstance().getDataFolder(), "locations.yml");
        configuration = YamlConfiguration.loadConfiguration(locations);

        configuration.set(p.getLocation().getWorld().getName() + ".world", p.getLocation().getWorld().getName());
        configuration.set(p.getWorld().getName() + ".spawn" + args[0] + ".x", p.getLocation().getX());
        configuration.set(p.getWorld().getName() + ".spawn" + args[0] + ".y", p.getLocation().getY());
        configuration.set(p.getWorld().getName() + ".spawn" + args[0] + ".z", p.getLocation().getZ());
        configuration.set(p.getWorld().getName() + ".spawn" + args[0] + ".pitch", p.getLocation().getPitch());
        configuration.set(p.getWorld().getName() + ".spawn" + args[0] + ".yaw", p.getLocation().getYaw());
        if(configuration.get(p.getWorld().getName() + ".name") == null) {
            configuration.set(p.getWorld().getName() + ".name", "Default");
        }
        if(configuration.get(p.getWorld().getName() + ".author") == null) {
            configuration.set(p.getWorld().getName() + ".author", "Default");
        }
        try{
            configuration.save(locations);
        }catch(Exception e) {
            e.printStackTrace();
        }
        p.sendMessage(getFormat("formats.successfully-set-map-spawnpoint", true, new String[] {"<spawnpoint>", args[0]}, new String[] {"<map>", p.getWorld().getName()}));
    }

}
