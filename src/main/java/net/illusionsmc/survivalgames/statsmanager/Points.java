package net.illusionsmc.survivalgames.statsmanager;

import net.illusionsmc.survivalgames.SurvivalGames;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

/**
 * Created by Joey on 7/31/2014.
 */
public class Points {

    private static File stats;
    private static FileConfiguration statsconfig;

    public static void addPoints(Player p, Integer amount) {
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        statsconfig.set(p.getName() + ".points", statsconfig.getInt(p.getName() + ".points") + amount);
        try{
            statsconfig.save(stats);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    public static Integer getPoints(String p) {
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        return (Integer)statsconfig.get(p + ".points");
    }
    public static void removePoints(Player p, Integer amount) {
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        statsconfig.set(p.getName() + ".points", statsconfig.getInt(p.getName() + ".points") - amount);
        try{
            statsconfig.save(stats);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

}
