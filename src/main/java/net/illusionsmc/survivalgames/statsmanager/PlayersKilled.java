package net.illusionsmc.survivalgames.statsmanager;

import net.illusionsmc.survivalgames.SurvivalGames;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

/**
 * Created by Joey on 7/31/2014.
 */
public class PlayersKilled {

    private static File stats;
    private static FileConfiguration statsconfig;

    public static void addPlayersKilled(Player p, Integer amount) {
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        statsconfig.set(p.getName() + ".playerskilled", statsconfig.getInt(p.getName() + ".playerskilled") + amount);
        try{
            statsconfig.save(stats);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    public static Integer getPlayersKilled(String p) {
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        return (Integer)statsconfig.get(p + ".playerskilled");
    }
    public static void removePlayersKilled(Player p, Integer amount) {
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        statsconfig.set(p.getName() + ".playerskilled", statsconfig.getInt(p.getName() + ".playerskilled") - amount);
        try{
            statsconfig.save(stats);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

}
