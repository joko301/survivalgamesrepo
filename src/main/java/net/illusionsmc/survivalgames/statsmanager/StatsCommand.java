package net.illusionsmc.survivalgames.statsmanager;

import com.portalmc.engine.command.Permission;
import com.portalmc.engine.command.PortalCommandHandler;
import net.illusionsmc.survivalgames.SurvivalGames;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

/**
 * Created by Joey on 7/31/2014.
 */
public class StatsCommand extends PortalCommandHandler {

    private static File stats;
    private static FileConfiguration statsconfig;

    public StatsCommand() {
        super("stats");
    }
    @Override
    protected void playerExecutesCommand(Player p, String[] args) {
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        if(args.length == 0) {
            p.sendMessage("§7§l§m---------------------------------------------");
            p.sendMessage(ChatColor.GOLD + p.getName() + "'s Stats");
            p.sendMessage(ChatColor.GRAY + "Games Won: " + ChatColor.YELLOW + GamesWon.getGamesWon(p.getName()));
            p.sendMessage(ChatColor.GRAY + "Players Killed: " + ChatColor.YELLOW + PlayersKilled.getPlayersKilled(p.getName()));
            p.sendMessage(ChatColor.GRAY + "Points: " + ChatColor.YELLOW + Points.getPoints(p.getName()));
            p.sendMessage("§7§l§m---------------------------------------------");
        }else if(args.length == 1) {
            if (statsconfig.getConfigurationSection(args[0]) == null) {
                p.sendMessage(ChatColor.RED + "Could not find player " + args[0] + " in our database.");
            } else {
                p.sendMessage("§7§l§m---------------------------------------------");
                p.sendMessage(ChatColor.GOLD + args[0] + "'s Stats");
                p.sendMessage(ChatColor.GRAY + "Games Won: " + ChatColor.YELLOW + GamesWon.getGamesWon(args[0]));
                p.sendMessage(ChatColor.GRAY + "Players Killed: " + ChatColor.YELLOW + PlayersKilled.getPlayersKilled(args[0]));
                p.sendMessage(ChatColor.GRAY + "Points: " + ChatColor.YELLOW + Points.getPoints(args[0]));
                p.sendMessage("§7§l§m---------------------------------------------");
            }
        }
    }

}
