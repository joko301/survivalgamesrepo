package net.illusionsmc.survivalgames.statsmanager;

import net.illusionsmc.survivalgames.SurvivalGames;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

/**
 * Created by Joey on 7/31/2014.
 */
public class GamesWon {

    private static File stats;
    private static FileConfiguration statsconfig;

    public static void addGamesWon(Player p, Integer amount) {
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        statsconfig.set(p.getName() + ".gameswon", statsconfig.getInt(p.getName() + ".gameswon") + amount);
        try{
            statsconfig.save(stats);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    public static Integer getGamesWon(String p) {
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        return (Integer)statsconfig.get(p + ".gameswon");
    }
    public static void removeGamesWon(Player p, Integer amount) {
        stats = new File(SurvivalGames.getInstance().getDataFolder(), "stats.yml");
        statsconfig = YamlConfiguration.loadConfiguration(stats);
        statsconfig.set(p.getName() + ".gameswon", statsconfig.getInt(p.getName() + ".gameswon") - amount);
        try{
            statsconfig.save(stats);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

}
