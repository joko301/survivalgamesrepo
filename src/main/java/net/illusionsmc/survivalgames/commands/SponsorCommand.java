package net.illusionsmc.survivalgames.commands;

import com.portalmc.engine.command.PortalCommandHandler;
import net.illusionsmc.survivalgames.utils.Spectator;
import net.illusionsmc.survivalgames.utils.SponsorManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 8/13/2014.
 */
public class SponsorCommand extends PortalCommandHandler {

    public SponsorCommand() {
        super("sponsor");
    }
    @Override
    protected void playerExecutesCommand(Player p, String[] args) {
        if(Spectator.spectators.contains(p)) {
            if (args.length < 1) {
                p.sendMessage(ChatColor.RED + "Not enough arguments.");
            }
            if (args.length == 1) {
                Player t = Bukkit.getPlayer(args[0]);
                if (SponsorManager.isSponsorAble(t)) {
                    Inventory sponsorMenu = Bukkit.createInventory(null, 9, "Sponsor Menu - " + t.getName());
                    p.openInventory(sponsorMenu);
                } else {
                    p.sendMessage(getFormat("formats.sponsors-off", true));
                }
            }
        }else{
            p.sendMessage(getFormat("formats.must-be-spectator", true));
        }
    }

}
