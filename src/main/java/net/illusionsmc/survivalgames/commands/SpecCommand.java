package net.illusionsmc.survivalgames.commands;

import com.portalmc.engine.command.PortalCommandHandler;
import net.illusionsmc.survivalgames.utils.GamePlayer;
import net.illusionsmc.survivalgames.utils.Spectator;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 7/28/2014.
 */
public class SpecCommand extends PortalCommandHandler {

    public SpecCommand() {
        super("spec");
    }

    @Override
    protected void playerExecutesCommand(Player p, String[] args) {
        if(!(Spectator.spectators.contains(p))) {
            p.sendMessage(getFormat("formats.must-be-spectator", true));
        }else {
            if (args.length == 0) {
                p.sendMessage(getFormat("formats.invalid-args", true, new String[] {"<usage>", "/[spec|spectate] <player>"}));
            } else if (args.length == 1) {
                Player t = Bukkit.getPlayer(args[0]);
                if (t == null) {
                    p.sendMessage(getFormat("formats.invalid-player", true));
                }
                if (Spectator.spectators.contains(p)) {
                    if (GamePlayer.players.contains(t)) {
                        p.teleport(t.getLocation());
                        p.sendMessage(getFormat("formats.now-spectating", true, new String[] {"<player>", t.getName()}));
                    } else {
                        p.sendMessage(getFormat("formats.not-playing", true));
                    }
                }
            }
        }
    }
    @Override
    protected List<String> tabCompleteArgs(CommandSender sender, String[] args) {
        List<String> messages = new ArrayList<>();
        for(Player pl : Bukkit.getOnlinePlayers()) {
            messages.add(pl.getName());
        }
        sender.sendMessage(String.valueOf(messages));
        return messages;
    }

}
