package net.illusionsmc.survivalgames.commands;

import com.portalmc.engine.command.Permission;
import com.portalmc.engine.command.PortalCommandHandler;
import net.illusionsmc.survivalgames.SurvivalGames;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 7/25/2014.
 */
@Permission("survivalgames.setlobby")
public class SetLobby extends PortalCommandHandler {

    private File locations;
    private FileConfiguration configuration;

    public SetLobby() {
        super("setlobby");
    }
    @Override
    protected void playerExecutesCommand(Player p, String[] args) {
        locations = new File(SurvivalGames.getInstance().getDataFolder(), "locations.yml");
        configuration = YamlConfiguration.loadConfiguration(locations);
        configuration.set("lobby.world", p.getLocation().getWorld().getName());
        configuration.set("lobby.x", p.getLocation().getX());
        configuration.set("lobby.y", p.getLocation().getY());
        configuration.set("lobby.z", p.getLocation().getZ());
        configuration.set("lobby.yaw", p.getLocation().getYaw());
        configuration.set("lobby.pitch", p.getLocation().getPitch());
        try{
            configuration.save(locations);
        }catch(Exception e) {
            e.printStackTrace();
        }
        p.sendMessage(getFormat("formats.lobby-set", true));
    }

}
