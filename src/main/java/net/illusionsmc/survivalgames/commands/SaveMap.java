package net.illusionsmc.survivalgames.commands;

import com.portalmc.engine.command.Permission;
import com.portalmc.engine.command.PortalCommandHandler;
import net.illusionsmc.survivalgames.utils.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import static net.illusionsmc.survivalgames.utils.Messages.getFormat;

/**
 * Created by Joey on 7/29/2014.
 */
@Permission("survivalgames.savemap")
public class SaveMap extends PortalCommandHandler {

    public SaveMap() {
        super("savemap");
    }

    @Override
    protected void playerExecutesCommand(Player p, String[] args) {
        World w = Bukkit.getWorld(MapManager.map);
        w.setAutoSave(false);
        w.save();
        p.sendMessage(getFormat("formats.map-saved", true, new String[] {"<map>", MapManager.map}));
        w.setAutoSave(false);
    }

}
