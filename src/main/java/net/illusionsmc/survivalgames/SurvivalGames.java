package net.illusionsmc.survivalgames;

import com.portalmc.engine.utils.PluginMeta;
import com.portalmc.engine.utils.PortalPlugin;
import lombok.Getter;
import lombok.SneakyThrows;
import net.illusionsmc.survivalgames.arena.SetSpawn;
import net.illusionsmc.survivalgames.commands.*;
import net.illusionsmc.survivalgames.events.*;
import net.illusionsmc.survivalgames.statsmanager.StatsCommand;
import net.illusionsmc.survivalgames.utils.GameState;
import net.illusionsmc.survivalgames.utils.MapManager;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.Arrays;

import static net.illusionsmc.survivalgames.timers.lobby.Lobby.startLobby;
import static net.illusionsmc.survivalgames.utils.MapManager.loadMaps;

/**
 * Created by Joey on 7/24/2014.
 */
@PluginMeta(name = "SurvivalGames", description = "SurvivalGames plugin for IllusionsMC", authors = {"joko301"}, website = "http://illusionsmc.net")
public final class SurvivalGames extends PortalPlugin {

    @Getter private static SurvivalGames instance;
    private File locations, stats;
    private FileConfiguration locconfig, statsconfig;

    @Override
    @SneakyThrows
    protected void enable() {
        instance = this;
        registerCommand(SetSpawn.class);
        registerCommand(SetLobby.class);
        registerCommand(SpecCommand.class);
        registerCommand(SaveMap.class);
        registerCommand(StatsCommand.class);
        registerCommand(SponsorCommand.class);
        registerListener(new JoinEvent());
        registerListener(new QuitEvent());
        registerListener(new MOTDHandler());
        registerListener(new PlayerMove());
        registerListener(new DeathEvent());
        registerListener(new LoginEvent());
        registerListener(new BlockHandler());
        registerListener(new PlayerUtils());
        registerListener(new ItemUtils());
        registerListener(new ChatHandler());
        registerListener(new SponsorToggle());
        registerListener(new MapVoter());
        registerListener(new PlayerNav());
        logInfoInColor(ChatColor.AQUA + "SurvivalGames has been successfully enabled.");
        loadMaps();
        GameState.setState(GameState.LOBBY);
        startLobby();

        locations = new File(getDataFolder(), "locations.yml");
        if(!locations.exists()) {
            try{
                locations.createNewFile();
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        locconfig = YamlConfiguration.loadConfiguration(locations);

        stats = new File(getDataFolder(), "stats.yml");
        if(!stats.exists()) {
            try{
                stats.createNewFile();
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        statsconfig = YamlConfiguration.loadConfiguration(stats);

        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }
    @Override
    protected void disable() {
        logInfoInColor(ChatColor.AQUA + "SurvivalGames has been successfully disabled.");
    }

    public static ItemStack createItem(Material mat, String name, String lore, Enchantment e, Integer enchantLevel) {
        ItemStack i = new ItemStack(mat);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(name);
        im.setLore(Arrays.asList(lore));
        im.addEnchant(e, enchantLevel, true);
        i.setItemMeta(im);
        return i;
    }
    public static ItemStack createItem(Material mat, String name, String lore) {
        ItemStack i = new ItemStack(mat);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(name);
        im.setLore(Arrays.asList(lore));
        i.setItemMeta(im);
        return i;
    }
    public static ItemStack createStainedGlass(Material mat, String name) {
        ItemStack i = new ItemStack(mat);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(name);
        i.setItemMeta(im);
        return i;
    }


}
